import passManager from './passManager';

class xr {
    static get basePath(){
        return passManager.LoadPassword();
    }

    static send(url, method, body) {
        if (!url.startsWith("http")) {
            url = xr.basePath + url;
        }

        return fetch(url, {
            method: method,
            body: body,
            headers: {
                'Content-Type': 'application/json'
            }
        });
    }

    static get(url) {
        return this.send(url, "GET");
    }

    static post(body, url = "/post") {
        return new Promise((res) => {
            this.send(url, "POST", JSON.stringify(body)).then(async (d) => {
                if (d.ok) {
                    try {
                        d = await d.clone().json();
                    }
                    catch {
                        d = await d.clone().text();
                    }
                    res(d);
                }
            });
        });
    }
}

export default xr;
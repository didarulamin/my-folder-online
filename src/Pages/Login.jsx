import { Component } from 'react';
import passManage from '../files/passManager';

class Login extends Component {

    constructor() {
        super();


        this.state = {
            rememberMe: false,
            password: '',
            wrongPass: false
        }

        this.SubmitInfo = this.SubmitInfo.bind(this);
    }

    async SubmitInfo(event) {
        event.preventDefault();
        this.setState({
            wrongPass: false
        });
        if(await passManage.setPassword(this.state.password, this.state.rememberMe)){
            this.props.history.push("/Files");
        } else {
            this.setState({
                wrongPass: true
            });
        }
    }

    render() {
        return (<div className="container-sm border p-2 rounded" style={{ maxWidth: '500px' }}>
            <form onSubmit={this.SubmitInfo}>
                <h2 className="text-center mb-4">Log in</h2>
                {this.state.wrongPass ? (<p className="text-danger m-2">Wrong password</p>): null}
                <div className="form-group d-flex">
                    <input type="password" onKeyUp={e => this.setState({ password: e.target.value })} className="form-control w-75 m-2" placeholder="Password" required="required" />
                    <button type="submit" className="btn btn-primary m-2" style={{minWidth: '70px'}}>Log in</button>
                </div>
                <div className="form-check m-2">
                    <input className="form-check-input" onClick={e => this.setState({ rememberMe: e.target.checked })} type="checkbox" id="flexCheckChecked" />
                    <label className="form-check-label" htmlFor="flexCheckChecked" >
                        Remember me
                    </label>
                </div>
            </form>
        </div>);
    }
}

export default Login;
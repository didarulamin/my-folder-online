import { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Zip from "./FilesComponents/Zip";
import Menu from './FilesComponents/Menu';
import Sticky from 'react-sticky-el';
import Preview from './FilesComponents/Preview';
import {v4} from 'uuid';

import xr from "../files/xr";

let files = [];

class Files extends Component {
    constructor() {
        super();

        const base = this;
        this.state = {
            name: '',
            inpath: "",
            files,
            loading: false,
            search: "",
            isSearch: false,
            menu: [{
                title: 'Open/Download',
                click() {
                    setTimeout(() => base.handleClick(base.SelectedIndex));
                }
            }, {
                title: 'Open containing folder',
                show: () => base.state.isSearch,
                click() {
                    setTimeout(() => base.updateFromServer(base.GetSelectedFile().path));
                }
            }, {
                title: 'Preview',
                show: () => (base.GetSelectedFile() || '').file,
                click() {
                    const info = base.GetSelectedFile();
                    base.setState({preview: {name: info.name, path: base.FullDownloadPath(base.getFullFilePath(info)), copyId: v4()}});
                }
            }],
            preview: {
                name: '',
                path: ''
            }
        }

        if (this.props && this.props.path) {
            this.state.inpath = this.props.path;
        }

        this.SearchInFiles = this.SearchInFiles.bind(this);
        this.UpFolder = this.UpFolder.bind(this);
        this.DownloadSelected = this.DownloadSelected.bind(this);
        this.CountSelected = this.CountSelected.bind(this);
        this.CreateZip = this.CreateZip.bind(this);
        this.MoveBackFolder = this.MoveBackFolder.bind(this);
        this.MoveNextFolder = this.MoveNextFolder.bind(this);
        this.keyAction = this.keyAction.bind(this);
        this.DisableKeyCapture = this.DisableKeyCapture.bind(this);
        this.EnableKeyCapture = this.EnableKeyCapture.bind(this);

        this.keyCapture = true;

        this.updateFromServer();

        this.StackBack = [{ folder: '' }];
        this.StackNext = [];

        window.addEventListener('keydown', this.keyAction);
    }

    updateCheckedFile(checked, index) {
        this.setState(prevState => {
            const UpdatedFiles = [...prevState.files];
            UpdatedFiles[index] = { ...prevState.files[index], select: checked }
            return { files: UpdatedFiles };
        });
    }

    LastPathName(text) {
        return text.substring(text.lastIndexOf('/') + 1);
    }

    async updateFromServer(folder = "", search = "", push = true) {

        if (push && (folder || search)) {
            this.StackBack.push({ folder, search });
        }

        this.setState({ isSearch: Boolean(search) });

        let end;
        setTimeout(() => {
            if (!end) {
                this.setState({ loading: true });
            }
        }, 200);

        const files = await xr.post({
            refer: "files",
            folder,
            search
        });

        end = true;

        this.setState({
            files,
            inpath: folder,
            loading: false,
            name: this.LastPathName(folder)
        });
    }

    MoveBackFolder() {
        let last = this.StackBack.pop();

        if (last) {
            let lastMore = last;
            last = this.StackBack.pop();

            if (!last) {
                last = lastMore;
            } else {
                this.StackNext.push(lastMore);
            }

            this.updateFromServer(last.folder, last.search, false);
            if (last.search) {
                this.setState({
                    search: last.search
                });
            }

            this.StackBack.push(last);
        }
    }

    MoveNextFolder() {
        let last = this.StackNext.pop();

        if (last) {

            this.updateFromServer(last.folder, last.search, true);

            if (last.search) {
                this.setState({
                    search: last.search
                });
            }
        }
    }

    FullDownloadPath(path){
        return xr.basePath + "/get?file=" + encodeURIComponent(path);
    }

    getFullFilePath(info){
        return (info.path || this.state.inpath) + "/" + info.name;
    }

    GetSelectedFile(){
        return this.state.files[this.SelectedIndex];
    }

    handleDownload(path) {
        const link = document.createElement('a');
        link.href = this.FullDownloadPath(path);
        link.download = this.LastPathName(path);
        link.click();
    }

    componentWillUnmount() {
        files = [...this.state.files];
    }

    handleClick(index) {
        const info = this.state.files[index];

        const path = this.getFullFilePath(info);
        if (info.file) {
            this.handleDownload(path);
        } else {
            this.updateFromServer(path);
        }
    }

    UpFolder() {
        const index = this.state.inpath.lastIndexOf('/');
        this.updateFromServer(this.state.inpath.substring(0, index));
    }

    onEnter(func) {
        return (event) => (event.keyCode === 13 ? func(event) : null);
    }

    SearchInFiles() {
        if (!this.state.search) {
            this.updateFromServer(this.state.inpath);
        } else {
            this.updateFromServer(this.state.inpath, this.state.search);
        }
    }

    getSplitPath(show) {
        const all = this.state.inpath.split('/');

        if (show) {
            all[0] = "Base folder";
        }

        return all;
    }

    openPath(index) {
        let newPath = "";

        for (const [key, value] of Object.entries(this.getSplitPath())) {
            newPath += value;

            if (key === String(index)) {
                break;
            }

            newPath += "/";
        }

        this.updateFromServer(newPath);
    }

    DownloadSelected() {
        toast.info(`Downloading ${this.CountSelected()} files, please wait...`);

        for (const i of this.state.files) {
            if (i.select) {
                this.handleDownload(this.getFullFilePath(i));
            }
        }
    }

    CountSelected() {
        let count = 0;

        for (const i of this.state.files) {
            if (i.select) {
                count++;
            }
        }

        return count;
    }

    CreateZip() {
        const allPaths = [];

        let last;
        for (const i of this.state.files) {
            if (i.select) {
                allPaths.push(i.name);
                last = (i.path || '') + i.name;
            }
        }

        this.zipCreator(allPaths, this.state.isSearch ? '.' : this.state.inpath, last);
    }

    SelectFuncInfo(func) {
        const AllFiles = [...this.state.files];
        for (const [key, value] of Object.entries(AllFiles)) {
            AllFiles[key] = { ...value, select: func(value) }
        }
        this.setState({
            files: AllFiles
        });
    }

    keyAction(event) {
        if (!this.keyCapture) {
            return;
        }

        if (event.code === 'KeyA' && event.ctrlKey) {
            this.SelectFuncInfo(() => true);
            event.preventDefault();
        } else if (event.code === 'Escape') {
            this.SelectFuncInfo(() => false);
            event.preventDefault();
        }
    }

    DisableKeyCapture() {
        this.keyCapture = false;
    }

    EnableKeyCapture() {
        this.keyCapture = true;
    }

    SetSelected(index, file) {
        this.SelectedIndex = index;

        const copy = [...this.state.menu];
        copy[0] = { ...copy[0], title: file ? 'Download' : 'Open' }

        this.setState({
            menu: copy
        });
    }

    render() {
        return (
            <>
                <Preview fileName={this.state.preview.name} filePath={this.state.preview.path} copyId={this.state.preview.copyId}/>
                <Zip ref={zip => this.zipCreator = zip && zip.AddZip}/>
                <ToastContainer />
                <Sticky>
                    <div className="tool p-2 border d-flex justify-content-between bg-white">
                        <div className="d-flex flex-wrap">
                            {this.state.loading ? (<div className="spinner-grow text-primary m-2" role="status"><span className="visually-hidden">Loading...</span></div>) : null}
                            <button className="m-2 btn btn-light" onClick={this.UpFolder}>⬆️</button>
                            <button className="m-2 btn btn-light" onClick={this.MoveNextFolder}>⬅️</button>
                            <button className="m-2 btn btn-light" onClick={this.MoveBackFolder}>➡️</button>
                            <button className="m-2 btn btn-light" disabled={!this.CountSelected()} onClick={this.DownloadSelected}>Download</button>
                            <button className="m-2 btn btn-light" disabled={!this.CountSelected()} onClick={this.CreateZip}>Download zip</button>
                        </div>
                        <div>
                            <div className="d-flex flex-wrap">
                                <input type="text" onFocus={this.DisableKeyCapture} onBlur={this.EnableKeyCapture} className="col m-2 d-inline-block form-control" style={{ maxWidth: '300px', minWidth: '100px'}} value={this.state.search} onKeyUp={this.onEnter(this.SearchInFiles)} onChange={e => this.setState({ search: e.target.value })} placeholder="Enter text to search" />
                                <button className="m-2 btn btn-outline-secondary" onClick={this.SearchInFiles}>Search</button>
                            </div>
                        </div>
                    </div>
                </Sticky>
                <p className="h2 mt-2 mb-2">{this.state.name}</p>
                {!this.state.isSearch ? <div className="border d-flex mt-2 mb-2 flex-wrap">
                    {this.getSplitPath(true).map((x, i) => <div className="d-flex" key={"path" + i}><div className="p-2 bg-light text-primary" onClick={() => this.openPath(i)} style={{ cursor: 'pointer' }}>{x}</div><div className="p-2 mb-1">&#62;</div></div>)}
                </div> : null}
                <div className="overflow-auto">
                    <Menu items={this.state.menu}>
                        <table className="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Select</th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.files.map(({ file, name, size, date, select }, index) => (
                                    <tr onContextMenu={() => this.SetSelected(index, file)} key={"files_" + index} onClick={() => this.updateCheckedFile(!select, index)}>
                                        <td >{index + 1}</td>
                                        <td>{file ? '📝' : '📁'}</td>
                                        <td className="text-center">{select ? '✔️' : '✖️'}</td>
                                        <td onDoubleClick={() => this.handleClick(index)} style={{ cursor: 'pointer' }}>{name}</td>
                                        <td>{size}</td>
                                        <td>{date}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </Menu>
                </div>
            </>
        );
    }
}

export default Files;
import { Menu, Item, useContextMenu } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.css';

const MENU_ID = "menu-files";

export default (function ({ children, items }){
    const { show } = useContextMenu({
        id: MENU_ID
    });

    return (<>
        <div onContextMenu={show}>
            {children}
        </div>
        <Menu id={MENU_ID}>
            {items.filter(x=> x.show === undefined || x.show() !== false).map(({title, click}) => (<Item key={"menu"+title} onClick={click}>{title}</Item>))}
        </Menu>
    </>
    );
});
import { Component } from 'react';
import passManage from '../files/passManager';
import PulseLoader from "react-spinners/PulseLoader";
import { css } from "@emotion/core";


class FastLink extends Component {
    constructor(props){
        super(props);

        this.override = css`
        display: block;
        margin: auto;
        margin-top: 50px;
        border-color: red;
        position: relative;
      `;
        
        this.CheckPassword();
    }

    async CheckPassword(){
        if(await passManage.setPassword(this.props.match.params.id)){
            this.props.history.push("/Files");
        } else {
            this.props.history.push("/Login");
        }
    }


    render() {
        return (<div className="text-center"><PulseLoader size={30} css={this.override} color="rgb(54 148 215)"/></div>);
    }
}

export default FastLink;